import React, { Component } from 'react';

import Login from './Login'
export default class Home  extends Component {
    render() {
        return (
                <div className="comp_home">
                  <h1>  Welcome to the Blood Donor Management Application!</h1> 
                  <div className="content">
                        <p>
                        During a regular donation, you will give around 470 ml of whole blood. This is about eight per cent of the average adult's blood volume. The body replaces this volume within 24 to 48 hours, and replenishes red blood cells in 10 to 12 weeks
                        </p>
                  </div>
                    <Login />
                </div>
        )
    }
}