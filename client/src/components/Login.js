import React, { Component } from 'react';
import axios from 'axios';

const api = axios.create({
    baseURL: 'http://localhost:8080',
})
const SUCCESS = 200;

export default class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password:'',
            isLoggedIn: false,
            showErrorMsg: false,
            errorMessage:''
        };
    }
    

    /* functions */

    loginHandler = (e)=>{
        e.preventDefault();
        console.log('loginHandler');
        let varUsername = this.state.username;
        let varPassword = this.state.password;

        if(varUsername === '' || varPassword === ''){
           this.errorMessageHandler(true,'Username/ password shouldn\'t be empty');
        }else{
            this.validateLoginActionHandler(varUsername,varPassword);
        }
    }

    validateLoginActionHandler = (username,password) => {
        console.log(`usernme:  ${username}`);
        console.log(`password: ${password}`);

        let userJSON = {
            "email": username,
            "password": password
        }
            api.post('/api/user/login',userJSON).then((result,err) => {
                if(result){
                    let resultData = result.data;
                   console.log(`result is ${JSON.stringify(result)}`);
                   console.log(`result Data is ${JSON.stringify(resultData)}`);

                    console.log(resultData["status"]);
                    if(resultData["status"] == SUCCESS){
                        console.log(JSON.stringify(resultData["accessToken"]));
                        localStorage.setItem('accessToken',JSON.stringify(resultData["payload"]["accessToken"]));
                        localStorage.setItem('userDetails',JSON.stringify(resultData["payload"]["user"]));
                        window.location.href = "/application";
                    }else{                
                         this.errorMessageHandler(true, resultData["message"]);
                    }
                }                
        }).catch((err)=>{
console.log(err);
        });
    
    }

    usernameTxtBoxHandler = (e)=>{
        this.errorMessageHandler(false,'');
        this.setState({
            username: e.target.value
        })
        
    }
    passwordTxtBoxHandler = (e)=>{
        this.errorMessageHandler(false,'');
        this.setState({
            password: e.target.value
        })
    }


    errorMessageHandler = (inputShowErrorMsg,inputErrorMessage)=>{
        this.setState({
            showErrorMsg: inputShowErrorMsg,
            errorMessage: inputErrorMessage,
        })
    }

    render() {
        return (
                <div className="comp_login">
                    {this.state.showErrorMsg &&(
<div className="empty_creds"> {this.state.errorMessage}  </div> 
                    )}
                    
                    <form className="login_form">
                        <h3>Log In</h3>
                        <input type="text" ref="username" onChange={this.usernameTxtBoxHandler} placeholder="Username" />
                        <input type="password" ref="password" onChange={this.passwordTxtBoxHandler} placeholder="Password" />
                        <input type="submit" value="Login"  onClick={this.loginHandler.bind()}/>
                    </form>
                </div>
        )
    }
}