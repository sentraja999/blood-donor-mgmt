const knex = require('knex');

const psql_config = {
    client: 'pg',
    connection: {
      host: 'localhost',
      database: 'blood_donors',
      user: 'postgres',
      password: 'postgres',
      port: '5433',

      // host: process.env.DB_HOST,
      // database: process.env.DB_NAME,
      // user: process.env.DB_USER,
      // password: process.env.DB_PASSWD,
      // port: process.env.PORT_NUMBER,
      // ssl: true,
      // dialect: 'postgres',
      // dialectOptions: {
      //   "ssl": {"require": true}
      // }
    }
  };
  
  // process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
  
  var psql_client = knex(psql_config);
  module.exports = psql_client;