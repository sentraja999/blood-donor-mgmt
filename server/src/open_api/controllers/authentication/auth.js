var jwt = require('jwt-simple');
var secretKey = require('../../controllers/authentication/secret');
var util = require('../../../common/utilities');
var constants = require('../../../common/constants');

var userRepo = require('../../../repositories/authentication/users');
const logger = require('../../../../logger');


var log = logger.LOG;
var auth = {
    login: function(req, res) {
        var email = req.body.email || '';
        var password = req.body.password || '';
        var timezone_offset = req.body.timezone_offset || 0;
        console.log(`input email ${email}`);
        console.log(`input password ${password}`);
        if(email ==='' || password ===''){
            let msg = "Failed to authenticate. email or password cannot be null";
            console.log(' +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ EMPTY credentials');
            log.debug('Error : k'+ msg);
            res.status(constants.http_status.UNAUTHORIZED);
            res.json({
                "status":constants.http_status.UNAUTHORIZED,
                "message":msg,
                "payload":null,
                "user_id":constants.user.DEFAULT_SUPER_ADMIN_USER_ID
            });
        }else{
            console.log(' +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ PASSEDD credentials');

            auth.validate(email, password,req.hostname,req.ip,function(err,data){

            console.log(' &&&&&&&&&&&&&&&&&&&&&&&&&& INSIDE validate');

                console.log('err');
                console.log(err);

                console.log('data');
                console.log(data);

                
                if(err){
                    // If authentication fails, we send a 401 back
                    // get a user record again
                    userRepo.findUserByEmail(email,function(users){

                        console.log('USERS '+users);
                        console.log(JSON.stringify(users));
                        if(!users || users.length <= 0){
                            // User name does not exist in the system!!!
                            console.log('user check 1');
                            let msg = "Failed to authenticate. Please check your email address and password";
                            log.debug('ERROR: Username doesn\'t exist in the system');

                            const content = {
                                "status":constants.http_status.UNAUTHORIZED,
                                "message":msg,
                                "payload":null
                            };
                            util.sendJSONresponse(res,constants.http_status.OK,content);
                        }else{
                            console.log('user check 2');
                            log.debug('users ==> '+JSON.stringify(users));
                            let msg = "Failed to authenticate. Please check your email address and password";
                            log.debug('msg: : '+msg);
                            const content ={
                                "status":constants.http_status.UNAUTHORIZED,
                                "message":msg,
                                "payload":null
                            };
                            util.sendJSONresponse(res,constants.http_status.OK,content);
                        }
                    });
                }else{
                    // The user was autheticated successfully      
                    // Does the user belong to this server. If not, redirect
                    // the login to the right server.
                    console.log('-----------------------else part-----------------');
                    if(data.user){
                        console.log('data.user');
                        console.log(data.user);
                        console.log(`JWT JWT JWT JWT JWT JWT JWT JWT JWT`);
                        const jwt = generateJWT(data.user,timezone_offset);
                        console.log(`JWT JWT JWT JWT JWT JWT JWT JWT JWT ${jwt}`);

                        console.log('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
                        const payload = util.decodeJWT(jwt);

                        console.log(`payload ${payload}`);
                        payload.accessToken = jwt;

                        
                        console.log(` payload.accessToken = jwt ${payload.accessToken}`);
                        console.log(` user id ${JSON.stringify(payload.user.id)} ---- created At ${JSON.stringify(payload.createdAt)}`);
                        
                        const key = util.createUserSessionKey(payload.user.id,payload.createdAt);

                        //const key = userId + '_@_' + timeStamp;

                        console.log(`key  ${key}`);

                        const userLoginInfo = {
                            "user:": payload.user,
                            "token":payload.accessToken,
                            "createdAt": payload.createdAt
                        };

                        res.status(constants.http_status.OK).json({
                            "status": constants.http_status.OK,
                            "message": null,
                            "payload": payload
                        });
                    }
                }
            });
        }
    },
    logout:function(req, res) {
        console.log('sssss');
        const key = util.getUserSessionKey(req);
        if(key)
            console.log(`key  ${key}`);
        if (key) {
            log.debug('Logging out user: '+key);
            userSessionCache.removeSession(key);
            userSessionCache.printStatus();
            res.status(constants.http_status.OK).json({
                "status": "SUCCESS",
                "message": "Successfully logging out a user.",
                "payload": {}
            });
        }
    },
    validate: function(email, password, hostname, remoteIP, callback) {
        console.log('inside validate in auth.js line 97');
        log.debug(`Authenticating user by  email: ${email}`);
      userRepo.authenticate(email, password, function(result) {
          console.log('result in auth.js line 116');
          console.log(JSON.stringify(result));
          const validUser = (result && result.length>0) || false;

          console.log(' -------------- VALID USER ----------------');
          console.log(validUser);

          console.log(` RESULT.LENGTH ::: ${result.length}`);
          if (validUser === true) {
              log.debug(`SUCCESS in authenticating user by email: ${email}`);

              const user = result[0];
              console.log(`%%%%%%%%%%%%%%%%%%   user ${JSON.stringify(user)}`);
              // const msp = user.user_type_id === constants.user.ADMIN_USER_TYPE_ID ||
                  //             user.user_type_id === constants.user.SUPER_USER_TYPE_ID;
                  // const redirect = isRedirect(hostname, user.redirected_server);
                  // if (!redirect) {
                  //     setUserLoggedInDetails(user, extractIPv4Field(remoteIP));
                  // }
                  callback(null, {
                      user: user
                  });
          } else {
              const msg = `FAILED in authenticating user by email: ${email}`;
              log.debug(msg);
              callback({msg: msg}, null);
          }
        })
    },


    
    // todo: function(req, res) {
    //     console.log(`TODO function is called to fetch all the TODO items.`)
    //     const data = [
    //         {
    //             "id": 1,
    //             "name": "Clear all the spam emails",
    //             "priority": "Critical",
    //             "category": "Home",
    //             "assignee": "Ahamed Kabeer",
    //             "status": "Open"
    //         },
    //         {
    //             "id": 2,
    //             "name": "Call Anumanth",
    //             "priority": "Critical",
    //             "category": "Home",
    //             "assignee": "Ahamed Kabeer",
    //             "status": "Open"
    //         },
    //         {
    //             "id": 3,
    //             "name": "Play badminton",
    //             "priority": "Medium",
    //             "category": "Home",
    //             "assignee": "Ahamed Kabeer",
    //             "status": "In Planning"
    //         },
    //         {
    //             "id": 4,
    //             "name": "Buy Groceries",
    //             "priority": "Medium",
    //             "category": "Home",
    //             "assignee": "Ahamed Kabeer",
    //             "status": "Open"
    //         },
    //         {
    //             "id": 5,
    //             "name": "Buy BackBags",
    //             "priority": "Medium",
    //             "category": "Home",
    //             "assignee": "Ahamed Kabeer",
    //             "status": "Open"
    //         },
    //         {
    //             "id": 6,
    //             "name": "Register Nominee for Insurance",
    //             "priority": "Critical",
    //             "category": "Home",
    //             "assignee": "Ahamed Kabeer",
    //             "status": "Open"
    //         },
    //         {
    //             "id": 7,
    //             "name": "Go to Gym",
    //             "priority": "Critical",
    //             "category": "Home",
    //             "assignee": "Ahamed Kabeer",
    //             "status": "Open"
    //         },
    //         {
    //             "id": 8,
    //             "name": "Work on labor law assignment",
    //             "priority": "Critical",
    //             "category": "Home",
    //             "assignee": "Ahamed Kabeer",
    //             "status": "Open"
    //         },
    //         {
    //             "id": 9,
    //             "name": "Participate in Football macth",
    //             "priority": "Critical",
    //             "category": "Home",
    //             "assignee": "Ahamed Kabeer",
    //             "status": "Open"
    //         },
    //         {
    //             "id": 10,
    //             "name": "Attend Technical seminar",
    //             "priority": "Critical",
    //             "category": "Home",
    //             "assignee": "Ahamed Kabeer",
    //             "status": "Open"
    //         },
    //         {
    //             "id": 11,
    //             "name": "Pay property tax",
    //             "priority": "Critical",
    //             "category": "Home",
    //             "assignee": "Ahamed Kabeer",
    //             "status": "Open"
    //         },
    //         {
    //             "id": 12,
    //             "name": "Collect Money from shops",
    //             "priority": "Critical",
    //             "category": "Home",
    //             "assignee": "Ahamed Kabeer",
    //             "status": "Open"
    //         },
    //         {
    //             "id": 13,
    //             "name": "Replace red color tiles by green",
    //             "priority": "Critical",
    //             "category": "Home",
    //             "assignee": "Ahamed Kabeer",
    //             "status": "Open"
    //         },
    //         {
    //             "id": 14,
    //             "name": "Repair iphone charger",
    //             "priority": "Critical",
    //             "category": "Home",
    //             "assignee": "Ahamed Kabeer",
    //             "status": "Open"
    //         },
    //         {
    //             "id": 15,
    //             "name": "Deploy smart recall product",
    //             "priority": "Critical",
    //             "category": "Home",
    //             "assignee": "Ahamed Kabeer",
    //             "status": "Open"
    //         },
    //         {
    //             "id": 16,
    //             "name": "Call friends",
    //             "priority": "Critical",
    //             "category": "Home",
    //             "assignee": "Ahamed Kabeer",
    //             "status": "Open"
    //         }
    //     ]
    //     const respObj = {
    //         status: 'SUCCESS',
    //         message: "All the TODO items fetched successfully",
    //         data: data
    //     };
    //     res.status(200).json(respObj);
    // }  
    
}

function generateJWT(user,timezone_offset) {
    return jwt.encode({
        exp:expiresIn(constants.auth.TOKEN_VALIDITY_SEVEN_DAYS),
        user:user,
        createdAt: new Date().getTime()
    },secretKey.getSecretKey());
}

function expiresIn(numDays) {
    var dateObj = new Date();
    return dateObj.setDate(dateObj.getDate() + numDays);
}

module.exports = auth;